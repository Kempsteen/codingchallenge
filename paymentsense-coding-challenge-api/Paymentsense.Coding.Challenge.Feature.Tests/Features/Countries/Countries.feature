﻿Feature: Countries
	Ability to retrieve various information about countries for a vaiaty of endpoints

Scenario: Can retrieve country overview
	Given a request for an overview of the countries
	When the country overview api is called
	Then the correct countries are returned

Scenario: Can retrieve detail about a specific country
	Given a request for the detail of Belgium
	When the country detail api is called
	Then the correct detail is returned