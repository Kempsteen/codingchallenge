﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.Services;
using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;
using Paymentsense.Coding.Challenge.Feature.Tests.Setup;
using TechTalk.SpecFlow;
using WireMock.RequestBuilders;

namespace Paymentsense.Coding.Challenge.Feature.Tests.Features.Countries
{
    [Binding]
    [Scope(Feature = "Countries")]
    public class CountriesFeatureSteps
    {
        private readonly HttpClient _httpClient;
        private readonly IJsonSerialisationService _jsonSerialisationService;

        private string _overviewRequestUrl;
        private HttpResponseMessage _countriesOverviewResponse;

        private string _detailRequestUrl;
        private HttpResponseMessage _detailResponse;

        public CountriesFeatureSteps()
        {
            _httpClient = TestHosting.Server.CreateClient();
            _jsonSerialisationService = new JsonSerialisationService();
        }

        [Given(@"a request for an overview of the countries")]
        public void GivenARequestForAnOverviewOfTheCountries()
        {
            _overviewRequestUrl = "api/countries";
        }

        [Given(@"a request for the detail of Belgium")]
        public void GivenARequestForTheDetailOfBelgium()
        {
            _detailRequestUrl = "api/countries/Belgium";
        }

        [When(@"the country detail api is called")]
        public async Task WhenTheCountryDetailApiIsCalled()
        {
            _detailResponse = await _httpClient.GetAsync(_detailRequestUrl);
        }

        [When(@"the country overview api is called")]
        public async Task WhenTheCountryOverviewApiIsCalled()
        {
            _countriesOverviewResponse = await _httpClient.GetAsync(_overviewRequestUrl);
        }

        [Then(@"the correct detail is returned")]
        public async Task ThenTheCorrectDetailIsReturned()
        {
            Assert.AreEqual(HttpStatusCode.OK, _detailResponse.StatusCode);

            var content = await _detailResponse.Content.ReadAsStringAsync();
            var typedResponseContent = _jsonSerialisationService.Deserialise<ApiResponse<GetCountryDetailQueryResponse>>(content);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(HttpStatusCode.OK, typedResponseContent.StatusCode);
                Assert.IsTrue(typedResponseContent.Succeeded, "The API call was not marked as succeeded.");
                Assert.AreEqual(11319511, typedResponseContent.Data.Country.Population);
                Assert.AreEqual("Brussels", typedResponseContent.Data.Country.Capital);
                CollectionAssert.AreEquivalent(new List<string> { "Dutch", "French", "German"}, typedResponseContent.Data.Country.Languages);
                CollectionAssert.AreEquivalent(new List<string> { "FRA", "DEU", "LUX", "NLD" }, typedResponseContent.Data.Country.Borders);

                var apiCallLog = WireMockService.Server.FindLogEntries(Request.Create().WithPath("/rest/v2/all").UsingGet());
                Assert.IsNotNull(apiCallLog);
                var callLogCount = apiCallLog.Count();
                Assert.AreEqual(1, callLogCount, $"There should have been 1 api call in the wire mock log, but there were '{callLogCount}'.");
            });
        }

        [Then(@"the correct countries are returned")]
        public async Task ThenTheCorrectCountriesAreReturned()
        {
            Assert.AreEqual(HttpStatusCode.OK, _countriesOverviewResponse.StatusCode);

            var content = await _countriesOverviewResponse.Content.ReadAsStringAsync();
            var typedResponseContent = _jsonSerialisationService.Deserialise<ApiResponse<GetCountriesQueryResponse>>(content);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(HttpStatusCode.OK, typedResponseContent.StatusCode);
                Assert.IsTrue(typedResponseContent.Succeeded, "The API call was not marked as succeeded.");
                Assert.AreEqual(250, typedResponseContent.Data.Countries.Count, 
                    $"There should have been 250 countries returned from the API but there were only: '{typedResponseContent.Data.Countries.Count}'.");
                Assert.AreEqual(250, typedResponseContent.Data.Countries.Select(e => e.Name).Distinct().Count(),
                    "There should have been 250 distinct countries returned.");
                Assert.AreEqual(250, typedResponseContent.Data.Countries.Select(e => e.FlagUrl).Distinct().Count(),
                    "There should have been 250 distinct flags returned.");

                var apiCallLog = WireMockService.Server.FindLogEntries(Request.Create().WithPath("/rest/v2/all").UsingGet());
                Assert.IsNotNull(apiCallLog);
                var callLogCount = apiCallLog.Count();
                Assert.AreEqual(1, callLogCount, $"There should have been 1 api call in the wire mock log, but there were '{callLogCount}'.");
            });
        }
    }
}