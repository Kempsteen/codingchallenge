﻿using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Paymentsense.Coding.Challenge.Feature.Tests.Setup;
using TechTalk.SpecFlow;

namespace Paymentsense.Coding.Challenge.Feature.Tests.Features.HealthCheck
{
    [Binding]
    [Scope(Feature = "Health Check")]
    public class HealthCheckFeatureSteps
    {
        private readonly HttpClient _httpClient;

        private HttpResponseMessage _healthCheckResponse;

        public HealthCheckFeatureSteps()
        {
            _httpClient = TestHosting.Server.CreateClient();
        }

        [Given(@"the system is running")]
        public void GivenTheSystemIsRunning()
        {
            // No action required for this step.
        }

        [When(@"calling the health check endpoint")]
        public async Task WhenCallingTheHealthCheckEndpoint()
        {
            _healthCheckResponse = await _httpClient.GetAsync("/health");
        }

        [Then(@"a healthy response should be returned")]
        public async Task ThenAHealthyResponseShouldBeReturned()
        {
            var responseString = await _healthCheckResponse.Content.ReadAsStringAsync();

            _healthCheckResponse.StatusCode.Should().Be(StatusCodes.Status200OK);
            responseString.Should().Be("Healthy");
        }
    }
}