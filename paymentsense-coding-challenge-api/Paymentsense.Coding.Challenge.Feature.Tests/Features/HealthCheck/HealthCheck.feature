﻿Feature: Health Check
	Endpoint for providing the health of the service

Scenario: Calling the health check endpoint returns healthy when the system is healthy
	Given the system is running
	When calling the health check endpoint
	Then a healthy response should be returned