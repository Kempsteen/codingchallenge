﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Paymentsense.Coding.Challenge.Api;

namespace Paymentsense.Coding.Challenge.Feature.Tests.Setup
{
    public static class TestHosting
    {
        private static TestServer server;

        public static TestServer Server => server ?? (server = new TestServer(GetWebHostBuilder()));

        public static void StartServer()
        {
            if (server != null)
            {
                server = new TestServer(GetWebHostBuilder());
            }
        }

        public static void StopServer()
        {
            if (server != null)
            {
                server.Dispose();
                server = null;
            }
        }

        public static T GetService<T>()
        {
            var service = (T)Server.Host.Services.GetService(typeof(T));

            return service;
        }

        private static IWebHostBuilder GetWebHostBuilder()
        {
            var settingPath = GetSettingsPath();

            var builder = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseConfiguration(new ConfigurationBuilder()
                    .AddJsonFile(settingPath)
                    .Build())
                .UseStartup<Startup>();

            return builder;
        }

        private static string GetSettingsPath()
        {
            return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\..\Paymentsense.Coding.Challenge.Api\appsettings.Test.json"));
        }
    }
}