﻿using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace Paymentsense.Coding.Challenge.Feature.Tests.Setup
{
    public static class WireMockService
    {
        public static WireMockServer Server;

        public static void Start()
        {
            Server = WireMockServer.Start(port:53144);

            // Default for API.
            Server.Given(Request.Create().WithPath("/rest/v2/all").UsingGet())
                .RespondWith(
                    Response.Create()
                    .WithStatusCode(200)
                    .WithHeader("Content-Type", "application/json")
                    .WithBodyFromFile("./StubData/ExternalCountriesApiStub.json"));
        }

        public static void Stop()
        {
            Server.Stop();
        }
    }
}