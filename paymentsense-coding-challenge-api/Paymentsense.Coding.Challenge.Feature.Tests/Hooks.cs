﻿using Paymentsense.Coding.Challenge.Feature.Tests.Setup;
using TechTalk.SpecFlow;

namespace Paymentsense.Coding.Challenge.Feature.Tests
{
    [Binding]
    public class Hooks
    {
        [BeforeTestRun]
        public static void Setup()
        {
            WireMockService.Start();
            TestHosting.StartServer();
        }

        [AfterTestRun]
        public static void Teardown()
        {
            TestHosting.StopServer();
            WireMockService.Stop();
        }
    }
}