﻿namespace Paymentsense.Coding.Challenge.Domain.CQRS
{
    /// <summary>
    ///     Defines a request with a response.
    /// </summary>
    /// <typeparam name="TResponse">The type of the response.</typeparam>
    public interface IRequest<TResponse> : IBaseRequest where TResponse : IResponse
    {

    }

    /// <summary>
    ///     Defines a request without a response.
    /// </summary>
    public interface IRequest : IBaseRequest
    {

    }

    public interface IBaseRequest
    {

    }
}