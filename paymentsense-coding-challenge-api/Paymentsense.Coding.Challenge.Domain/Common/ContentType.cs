﻿namespace Paymentsense.Coding.Challenge.Domain.Common
{
    public static class ContentType
    {
        public const string ApplicationJson = "application/json";
    }
}