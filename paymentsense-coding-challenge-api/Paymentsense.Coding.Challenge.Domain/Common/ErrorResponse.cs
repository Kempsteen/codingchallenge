﻿using System.Collections.Generic;

namespace Paymentsense.Coding.Challenge.Domain.Common
{
    public class ErrorResponse
    {
        public ErrorResponse()
        {
        }

        public ErrorResponse(string exceptionType, IList<Error> errors)
        {
            Errors = errors;
            ExceptionType = exceptionType;
        }

        public ErrorResponse(string exceptionType, Error error)
        {
            Errors = new List<Error> { error };
            ExceptionType = exceptionType;
        }

        public string ExceptionType { get; set; }
        public IList<Error> Errors { get; set; }
    }
}