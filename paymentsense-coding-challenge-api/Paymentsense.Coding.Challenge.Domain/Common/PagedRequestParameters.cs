﻿namespace Paymentsense.Coding.Challenge.Domain.Common
{
    public class PagedRequestParameters
    {
        public int PageNumber { get; set; } = 1;
        public int ItemsPerPage { get; set; } = 25;
    }
}