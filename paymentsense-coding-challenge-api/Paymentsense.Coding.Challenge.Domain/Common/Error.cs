﻿using System;

namespace Paymentsense.Coding.Challenge.Domain.Common
{
    public class Error
    {
        public Error(string fullErrorMessage)
        {
            // TODO: Add more validation here to ensure message format.
            Code = fullErrorMessage.Substring(1, 4);
            Message = fullErrorMessage.Substring(7, fullErrorMessage.Length - 7);
        }

        public Error(string code, string message)
        {
            if (code == null || code.Length != 4)
            {
                throw new ArgumentException("Code must have length of 4 and only contain numeric characters.", nameof(code));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("A message is required to have some text.", nameof(message));
            }

            Code = code;
            Message = message;
        }

        public string Code { get; set; }
        public string Message { get; set; }

        public string FullMessage => $"[{Code}] {Message}";
    }
}