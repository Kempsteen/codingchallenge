﻿using System.Net;

namespace Paymentsense.Coding.Challenge.Domain.Common
{
    public class ApiResponse<T> : ApiResponse
    {
        public T Data { get; set; }

        public static ApiResponse Ok(T data)
        {
            return new ApiResponse<T> { StatusCode = HttpStatusCode.OK, Data = data };
        }
    }

    public class ApiResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public bool Succeeded => (int)StatusCode >= 200 && (int)StatusCode < 300;
        public ErrorResponse ErrorResponse { get; set; }

        public static ApiResponse Ok()
        {
            return new ApiResponse { StatusCode = HttpStatusCode.OK };
        }
    }
}