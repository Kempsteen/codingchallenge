﻿namespace Paymentsense.Coding.Challenge.Domain.Common
{
    public abstract class PagedResponse
    {
        public int PageNumber { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }

        public int TotalPages
        {
            get
            {
                var fraction = (double) TotalItems / ItemsPerPage;
                var hasLeftOver = (double) TotalItems % ItemsPerPage > 0;

                return hasLeftOver ? (int)fraction + 1 : (int) fraction;
            }
        }

        public int ItemsFrom => ItemsPerPage * (PageNumber - 1) + 1;

        public int ItemsTo
        {
            get
            {
                var itemsTo = ItemsPerPage * PageNumber;
                return TotalItems > itemsTo ? itemsTo : TotalItems;
            }
        }
    }
}