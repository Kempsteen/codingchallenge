﻿using System.Net;
using Paymentsense.Coding.Challenge.Domain.Common;

namespace Paymentsense.Coding.Challenge.Domain
{
    public static class ErrorMessages
    {
        public static Error ExternalCountryApiFailed(HttpStatusCode statusCode, string body)
        {
            return new Error("0001", $"External call to rest countries API failed with status code: '{statusCode}' and body '{body}'");
        }

        public static Error PageNumberMustBePositive()
        {
            return new Error("0002", "Page number must be greater than 0.");
        }

        public static Error ItemsPerPageMustBePositive()
        {
            return new Error("0003", "Items per page must be greater than 0.");
        }

        public static Error CountryDetailForNameCouldNotBeFound(string countryName)
        {
            return new Error("0004", $"No country could be found for name '{countryName}'");
        }
    }
}