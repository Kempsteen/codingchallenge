﻿using Paymentsense.Coding.Challenge.Domain.CQRS;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries.Models;

namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries
{
    public class GetCountryDetailQueryResponse : IResponse
    {
        public CountryDetailDto Country { get; set; }
    }
}