﻿using System.Collections.Generic;
using Paymentsense.Coding.Challenge.Domain.CQRS;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries.Models;

namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries
{
    public class GetCountriesQueryResponse : IResponse
    {
        public IList<CountryOverviewDto> Countries { get; set; }
    }
}