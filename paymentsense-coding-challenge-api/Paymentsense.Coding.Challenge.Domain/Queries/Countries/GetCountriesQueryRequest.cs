﻿using Paymentsense.Coding.Challenge.Domain.CQRS;

namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries
{
    public class GetCountriesQueryRequest : IRequest<GetCountriesQueryResponse>
    {
        
    }
}