﻿using System.Collections.Generic;

namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries.Models
{
    public class CountryDetailDto
    {
        public string Name { get; set; }
        public string FlagUrl { get; set; }
        public int Population { get; set; }
        public IList<string> Languages { get; set; }
        public string Capital { get; set; }
        public IList<string> Borders { get; set; }
    }
}