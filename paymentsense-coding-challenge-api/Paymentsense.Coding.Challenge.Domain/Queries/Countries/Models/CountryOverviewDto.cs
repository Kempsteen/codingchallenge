﻿namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries.Models
{
    public class CountryOverviewDto
    {
        public string Name { get; set; }
        public string FlagUrl { get; set; }
    }
}