﻿using Paymentsense.Coding.Challenge.Domain.CQRS;

namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries
{
    public class GetPaginatedCountriesQueryRequest : IRequest<GetPaginatedCountriesQueryResponse>
    {
        public int PageNumber { get; set; }
        public int ItemsPerPage { get; set; }
    }
}