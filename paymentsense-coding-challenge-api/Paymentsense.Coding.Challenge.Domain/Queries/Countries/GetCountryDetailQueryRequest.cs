﻿using Paymentsense.Coding.Challenge.Domain.CQRS;

namespace Paymentsense.Coding.Challenge.Domain.Queries.Countries
{
    public class GetCountryDetailQueryRequest : IRequest<GetCountryDetailQueryResponse>
    {
        public string CountryName { get; set; }
    }
}