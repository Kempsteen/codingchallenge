﻿using System;
using System.Collections.Generic;
using System.Linq;
using Paymentsense.Coding.Challenge.Domain.Common;

namespace Paymentsense.Coding.Challenge.Domain.Exceptions
{
    public class ExternalSystemException : Exception
    {
        public IList<Error> Errors;

        public ExternalSystemException(Error error) : base(error.FullMessage)
        {
            Errors = new List<Error> { error };
        }

        public ExternalSystemException(IList<Error> errors) : base(string.Join(", ", errors.Select(e => e.FullMessage)))
        {
            Errors = errors;
        }

        public ExternalSystemException()
        {
        }

        public ExternalSystemException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}