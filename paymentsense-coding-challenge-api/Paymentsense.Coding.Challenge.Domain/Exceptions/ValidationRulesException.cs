﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using Paymentsense.Coding.Challenge.Domain.Common;

namespace Paymentsense.Coding.Challenge.Domain.Exceptions
{
    public class ValidationRulesException : Exception
    {
        public IList<Error> Errors;

        public ValidationRulesException(IList<ValidationFailure> errors)
        {
            Errors = errors.Select(e => new Error(e.ErrorMessage)).ToList();
        }

        public ValidationRulesException(Error error) : base(error.FullMessage)
        {
            Errors = new List<Error> { error };
        }

        public ValidationRulesException(IList<Error> errors) : base(string.Join(", ", errors.Select(e => e.FullMessage)))
        {
            Errors = errors;
        }

        public ValidationRulesException()
        {
        }

        public ValidationRulesException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}