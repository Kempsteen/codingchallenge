﻿using System;
using Paymentsense.Coding.Challenge.Domain.Common;

namespace Paymentsense.Coding.Challenge.Domain.Exceptions
{
    public class DataNotFoundException : Exception
    {
        public string Id;
        public Type Type;
        public Error Error;

        public DataNotFoundException(string id, Type dataType, Error error)
            : base(error.FullMessage)
        {
            Id = id;
            Type = dataType;
            Error = error;
        }

        public DataNotFoundException()
        {
        }

        public DataNotFoundException(string message) : base(message)
        {
        }

        public DataNotFoundException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}