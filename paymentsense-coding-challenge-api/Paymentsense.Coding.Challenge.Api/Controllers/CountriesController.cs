﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Paymentsense.Coding.Challenge.Core.Handlers;
using Paymentsense.Coding.Challenge.Core.Mapping.Countries;
using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Api.Controllers
{
    /// <summary>
    ///     Services requests for country resources.
    /// </summary>
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ILogger<CountriesController> _logger;

        private readonly IRequestHandler<GetCountriesQueryRequest, GetCountriesQueryResponse> _getCountriesHandler;
        private readonly IRequestHandler<GetPaginatedCountriesQueryRequest, GetPaginatedCountriesQueryResponse> _getPaginatedCountriesHandler;
        private readonly IRequestHandler<GetCountryDetailQueryRequest, GetCountryDetailQueryResponse> _countryDetailHandler;

        public CountriesController(
            ILogger<CountriesController> logger, 
            IRequestHandler<GetCountriesQueryRequest, GetCountriesQueryResponse> getCountriesHandler, 
            IRequestHandler<GetPaginatedCountriesQueryRequest, GetPaginatedCountriesQueryResponse> getPaginatedCountriesHandler, 
            IRequestHandler<GetCountryDetailQueryRequest, GetCountryDetailQueryResponse> countryDetailHandler)
        {
            _logger = logger;
            _getCountriesHandler = getCountriesHandler;
            _getPaginatedCountriesHandler = getPaginatedCountriesHandler;
            _countryDetailHandler = countryDetailHandler;
        }

        /// <summary>
        ///     Retrieves the detail for a specific country.
        /// </summary>
        /// <response code="500">If the service is unavailable</response>
        /// <response code="404">If the specific country cannot be found</response>
        /// <response code="400">If the request is not valid</response>
        /// <response code="200">If request is successful</response>
        [HttpGet]
        [Route("api/countries/{countryName}")]
        [Produces(ContentType.ApplicationJson)]
        [Consumes(ContentType.ApplicationJson)]
        [ProducesResponseType(typeof(ApiResponse<GetCountryDetailQueryResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<ApiResponse<GetCountryDetailQueryResponse>>> GetCountryDetailAsync([FromRoute] string countryName)
        {
            _logger.LogDebug($"Request received for detail of country '{countryName}'");

            var response = await _countryDetailHandler.HandleAsync(new GetCountryDetailQueryRequest { CountryName = countryName });

            return Ok(ApiResponse<GetCountryDetailQueryResponse>.Ok(response));
        }

        /// <summary>
        ///     Retrieves an overview of all the countries.
        /// </summary>
        /// <response code="500">If the service is unavailable</response>
        /// <response code="200">If request is successful</response>
        [HttpGet]
        [Route("api/countries")]
        [Produces(ContentType.ApplicationJson)]
        [Consumes(ContentType.ApplicationJson)]
        [ProducesResponseType(typeof(ApiResponse<GetCountriesQueryResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<ApiResponse<GetCountriesQueryResponse>>> GetCountriesAsync()
        {
            _logger.LogDebug("Request received for overview of countries");

            var response = await _getCountriesHandler.HandleAsync(new GetCountriesQueryRequest());

            _logger.LogDebug("Request returned '{CountryCount}' countries", response.Countries.Count);

            return Ok(ApiResponse<GetCountriesQueryResponse>.Ok(response));
        }

        /// <summary>
        ///     Retrieves an overview of countries with pagination.
        /// </summary>
        /// <response code="500">If the service is unavailable</response>
        /// <response code="200">If request is successful</response>
        /// <response code="400">If the request is not valid</response>
        [HttpGet]
        [Route("api/countries/paginated")]
        [Produces(ContentType.ApplicationJson)]
        [Consumes(ContentType.ApplicationJson)]
        [ProducesResponseType(typeof(ApiResponse<GetPaginatedCountriesQueryResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<ApiResponse<GetPaginatedCountriesQueryResponse>>> GetPaginatedCountriesAsync([FromQuery] PagedRequestParameters parameters)
        {
            _logger.LogDebug("Request received for paginated overview of countries with page number '{PageNumber}' and items per page '{ItemsPerPage}'",
                parameters.PageNumber,
                parameters.ItemsPerPage);

            var request = parameters.ToGetPaginatedCountriesQueryRequest();

            var response = await _getPaginatedCountriesHandler.HandleAsync(request);

            _logger.LogDebug("Request returned '{CountryCount}' countries", response.Countries.Count);

            return Ok(ApiResponse<GetPaginatedCountriesQueryResponse>.Ok(response));
        }
    }
}