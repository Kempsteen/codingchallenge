﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Paymentsense.Coding.Challenge.Core.Services;
using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Exceptions;

namespace Paymentsense.Coding.Challenge.Api.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;
        private readonly IJsonSerialisationService _serialiser;

        /// <summary>
        /// Constructs an Error Handling Middleware instance.
        /// </summary>
        /// <param name="next">The next request delegate in the pipeline.</param>
        /// <param name="logger">A logger instance.</param>
        /// <param name="serialiser">A Json serializer.</param>
        public ExceptionHandlingMiddleware(
            RequestDelegate next,
            ILogger<ExceptionHandlingMiddleware> logger,
            IJsonSerialisationService serialiser)
        {
            _next = next;
            _logger = logger;
            _serialiser = serialiser;
        }

        /// <summary>
        /// Invokes the next delegate in the chain, processing any exceptions that occur and handling appropriately.
        /// </summary>
        /// <param name="context">The current http request context.</param>
        /// <returns>A Task.</returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code;
            string result;

            switch (exception)
            {
                case ExternalSystemException externalSystemException:
                    code = HttpStatusCode.InternalServerError;
                    result = _serialiser.Serialise(
                        new ApiResponse
                        {
                            StatusCode = code,
                            ErrorResponse = new ErrorResponse(nameof(ExternalSystemException), externalSystemException.Errors)
                        });
                    _logger.LogWarning("An external system exception was thrown with response: [{@Response}].", result);
                    break;

                case ValidationRulesException validationException:
                    code = HttpStatusCode.BadRequest;
                    result = _serialiser.Serialise(
                        new ApiResponse
                        {
                            StatusCode = code,
                            ErrorResponse = new ErrorResponse(nameof(ValidationRulesException), validationException.Errors)
                        });
                    _logger.LogWarning("A validation rules exception was thrown with response: [{@Response}].", result);
                    break;

                default:
                    var type = exception.GetType();
                    code = HttpStatusCode.InternalServerError;
                    result = _serialiser.Serialise(
                        new ApiResponse<object>
                        {
                            StatusCode = code,
                            Data = null,
                            ErrorResponse = new ErrorResponse(type.Name, new Error("9999", exception.Message))
                        });
                    _logger.LogError("An unknown exception was thrown with response: [{@Response}].", result);
                    break;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            await context.Response.WriteAsync(result);
        }
    }
}