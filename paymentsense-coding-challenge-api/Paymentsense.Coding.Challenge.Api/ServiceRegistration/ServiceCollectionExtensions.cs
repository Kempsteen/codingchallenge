﻿using System;
using FluentValidation;
using LazyCache;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Handlers;
using Paymentsense.Coding.Challenge.Core.Handlers.Countries;
using Paymentsense.Coding.Challenge.Core.Services;
using Paymentsense.Coding.Challenge.Core.Validators.Countries;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Api.ServiceRegistration
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterHttpClients(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddHttpClient<ICountryApiHttpClient, CountryApiHttpClient>(client =>
            {
                client.BaseAddress = new Uri(configuration.GetValue<string>("ExternalApiServiceUrl"));
            });
        }

        public static void RegisterValidators(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IValidator<GetPaginatedCountriesQueryRequest>, GetPaginatedCountriesQueryRequestValidator>();
        }

        public static void RegisterServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddTransient<IJsonSerialisationService, JsonSerialisationService>();

            var cachingEnabled = configuration.GetValue<bool>("UseCaching");

            if (cachingEnabled)
            {
                serviceCollection.AddTransient<ICountryApiService, CachedCountryApiService>();
            }
            else
            {
                serviceCollection.AddTransient<ICountryApiService, CountryApiService>();
            }

            serviceCollection.AddTransient<IAppCache>(provider => new CachingService());
        }

        public static void RegisterHandlers(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IRequestHandler<GetCountriesQueryRequest, GetCountriesQueryResponse>, CountryOverviewHandler>();
            serviceCollection.AddTransient<IRequestHandler<GetPaginatedCountriesQueryRequest, GetPaginatedCountriesQueryResponse>, PaginatedCountryOverviewHandler>();
            serviceCollection.AddTransient<IRequestHandler<GetCountryDetailQueryRequest, GetCountryDetailQueryResponse>, CountryDetailHandler>();
            
        }
    }
}