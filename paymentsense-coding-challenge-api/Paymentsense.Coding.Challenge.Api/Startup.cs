using System;
using System.IO;
using System.Text.Json;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Paymentsense.Coding.Challenge.Api.Middleware;
using Paymentsense.Coding.Challenge.Api.ServiceRegistration;

namespace Paymentsense.Coding.Challenge.Api
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;

        public Startup(
            IConfiguration configuration, 
            IWebHostEnvironment environment)
        {
            Configuration = configuration;
            _environment = environment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddFluentValidation(fv =>
                {
                    fv.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                    fv.ImplicitlyValidateChildProperties = false;
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                });


            services.AddHealthChecks();

            services.AddCors(options =>
            {
                options.AddPolicy("PaymentsenseCodingChallengeOriginPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            services.RegisterServices(Configuration);
            services.RegisterHttpClients(Configuration);
            services.RegisterHandlers();
            services.RegisterValidators();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Leon Kemp Paymentsense Solution Api", Version = "v1" });

                try
                {
                    var basePath = _environment.ContentRootPath;

                    var directoryInfo = new DirectoryInfo(basePath).GetFiles("Paymentsense.*.xml").Length > 0
                        ? new DirectoryInfo(basePath)
                        : new DirectoryInfo(Path.Combine(basePath, @"bin\Debug\netcoreapp3.0"));

                    foreach (var file in directoryInfo.EnumerateFiles("Paymentsense.*.xml"))
                    {
                        c.IncludeXmlComments(file.FullName);
                    }
                }
                catch (Exception)
                {
                   // Ignore, just can't load documentation to API.
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Leon Kemp Paymentsense Solution Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseCors("PaymentsenseCodingChallengeOriginPolicy");

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}
