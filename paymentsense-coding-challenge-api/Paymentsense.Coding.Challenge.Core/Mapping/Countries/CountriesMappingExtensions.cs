﻿using System.Collections.Generic;
using System.Linq;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries.Models;

namespace Paymentsense.Coding.Challenge.Core.Mapping.Countries
{
    public static class CountriesMappingExtensions
    {
        public static IList<CountryOverviewDto> ToCountryOverviewDtoCollection(this IList<CountryDetailsModel> countryModels)
        {
            var models = countryModels.Select(ToCountryOverviewDto).ToList();

            return models;
        }

        public static CountryOverviewDto ToCountryOverviewDto(this CountryDetailsModel model)
        {
            return new CountryOverviewDto
            {
                Name = model.Name,
                FlagUrl = model.Flag
            };
        }

        public static CountryDetailDto ToCountryDetailDto(this CountryDetailsModel model)
        {
            return new CountryDetailDto
            {
                Name = model.Name,
                FlagUrl = model.Flag,
                Capital = model.Capital,
                Population = model.Population,
                Languages = model.Languages.Select(e => e.Name).ToList(),
                Borders = model.Borders
            };
        }
    }
}