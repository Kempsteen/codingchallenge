﻿using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Core.Mapping.Countries
{
    public static class PaginatedCountriesMappingExtensions
    {
        public static GetPaginatedCountriesQueryRequest ToGetPaginatedCountriesQueryRequest(this PagedRequestParameters parameters)
        {
            return new GetPaginatedCountriesQueryRequest
            {
                ItemsPerPage = parameters.ItemsPerPage,
                PageNumber = parameters.PageNumber
            };
        }
    }
}