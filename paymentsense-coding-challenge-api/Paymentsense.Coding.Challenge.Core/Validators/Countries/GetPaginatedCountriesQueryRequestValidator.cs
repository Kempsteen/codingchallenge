﻿using FluentValidation;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Core.Validators.Countries
{
    public class GetPaginatedCountriesQueryRequestValidator : AbstractValidator<GetPaginatedCountriesQueryRequest>
    {
        public GetPaginatedCountriesQueryRequestValidator()
        {
            RuleFor(e => e.PageNumber).GreaterThan(0).WithMessage(ErrorMessages.PageNumberMustBePositive().FullMessage);
            RuleFor(e => e.ItemsPerPage).GreaterThan(0).WithMessage(ErrorMessages.ItemsPerPageMustBePositive().FullMessage);
        }
    }
}