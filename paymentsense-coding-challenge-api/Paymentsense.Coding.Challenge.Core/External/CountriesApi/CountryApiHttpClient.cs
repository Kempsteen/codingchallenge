﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi
{
    public class CountryApiHttpClient : ICountryApiHttpClient
    {
        private readonly HttpClient _httpClient;

        public CountryApiHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetCountriesAsync()
        {
            return await _httpClient.GetAsync("rest/v2/all").ConfigureAwait(false);
        }
    }
}