﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Logging;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models;
using Paymentsense.Coding.Challenge.Core.Services;

namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi
{
    public class CachedCountryApiService : CountryApiService
    {
        private readonly IAppCache _cache;
        private readonly ILogger<CountryApiService> _logger;

        public CachedCountryApiService(
            ICountryApiHttpClient countryApiClient, 
            IJsonSerialisationService serialisationService, 
            ILogger<CountryApiService> logger, 
            IAppCache cache) : base(countryApiClient, serialisationService, logger)
        {
            _cache = cache;
            _logger = logger;
        }

        public override async Task<IList<CountryDetailsModel>> GetAllCountriesAsync()
        {
            _logger.LogDebug("Cached country api service was called for the countries.");

            var countries = await _cache.GetOrAddAsync(nameof(GetAllCountriesAsync), () => base.GetAllCountriesAsync());

            return countries;
        }
    }
}