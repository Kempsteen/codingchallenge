﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models;

namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi
{
    public interface ICountryApiService
    {
        Task<IList<CountryDetailsModel>> GetAllCountriesAsync();
    }
}