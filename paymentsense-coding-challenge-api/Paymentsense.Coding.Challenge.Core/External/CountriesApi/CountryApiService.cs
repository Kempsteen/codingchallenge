﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models;
using Paymentsense.Coding.Challenge.Core.Services;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Exceptions;

namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi
{
    public class CountryApiService : ICountryApiService
    {
        private readonly ICountryApiHttpClient _countryApiClient;
        private readonly IJsonSerialisationService _serialisationService;

        private readonly ILogger<CountryApiService> _logger;

        public CountryApiService(
            ICountryApiHttpClient countryApiClient, 
            IJsonSerialisationService serialisationService, 
            ILogger<CountryApiService> logger)
        {
            _countryApiClient = countryApiClient;
            _serialisationService = serialisationService;
            _logger = logger;
        }

        public virtual async Task<IList<CountryDetailsModel>> GetAllCountriesAsync()
        {
            var countriesResponse = await _countryApiClient.GetCountriesAsync().ConfigureAwait(false);

            if (!countriesResponse.IsSuccessStatusCode)
            {
                throw await HandleApiExceptionAsync(countriesResponse).ConfigureAwait(false);
            }

            _logger.LogDebug("Countries successfully retrieved from external API service.");

            var bodyContent = await countriesResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

            return _serialisationService.Deserialise<IList<CountryDetailsModel>>(bodyContent);
        }

        private async Task<ExternalSystemException> HandleApiExceptionAsync(HttpResponseMessage countriesResponse)
        {
            var failureContent = await countriesResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

            var exception = new ExternalSystemException(ErrorMessages.ExternalCountryApiFailed(countriesResponse.StatusCode, failureContent));

            _logger.LogError(exception, "External API call to retrieve countries failed.");

            return exception;
        }
    }
}