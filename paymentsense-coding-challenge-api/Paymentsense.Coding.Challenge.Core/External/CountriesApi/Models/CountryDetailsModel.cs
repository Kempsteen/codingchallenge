﻿using System.Collections.Generic;

namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models
{
    public class CountryDetailsModel
    {
        public string Name { get; set; }
        public string Flag { get; set; }
        public int Population { get; set; }
        public IList<LanguageModel> Languages { get; set; }
        public string Capital { get; set; }
        public IList<string> Borders { get; set; }
    }
}