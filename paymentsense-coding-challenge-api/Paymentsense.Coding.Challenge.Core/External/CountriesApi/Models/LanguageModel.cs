﻿namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models
{
    public class LanguageModel
    {
        public string Name { get; set; }
    }
}