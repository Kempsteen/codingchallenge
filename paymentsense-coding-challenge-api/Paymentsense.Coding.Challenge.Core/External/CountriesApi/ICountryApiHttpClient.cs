﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Paymentsense.Coding.Challenge.Core.External.CountriesApi
{
    public interface ICountryApiHttpClient
    {
        Task<HttpResponseMessage> GetCountriesAsync();
    }
}