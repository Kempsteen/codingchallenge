﻿using System.Threading.Tasks;
using Paymentsense.Coding.Challenge.Domain.CQRS;

namespace Paymentsense.Coding.Challenge.Core.Handlers
{
    /// <summary>
    ///     Handles a request with an associated response type (Query)
    /// </summary>
    /// <typeparam name="TRequest"></typeparam>
    /// <typeparam name="TResponse"></typeparam>
    public interface IRequestHandler<in TRequest, TResponse>
        where TRequest : IRequest<TResponse>
        where TResponse : IResponse
    {
        Task<TResponse> HandleAsync(TRequest request);
    }

    /// <summary>
    ///     Handles a request with no associated response (Command).
    /// </summary>
    /// <typeparam name="TRequest">The type of the request</typeparam>
    public interface IRequestHandler<in TRequest>
        where TRequest : IRequest
    {
        Task HandleAsync(TRequest request);
    }
}