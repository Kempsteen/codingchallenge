﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Mapping.Countries;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Exceptions;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries.Models;

namespace Paymentsense.Coding.Challenge.Core.Handlers.Countries
{
    public class CountryDetailHandler : IRequestHandler<GetCountryDetailQueryRequest, GetCountryDetailQueryResponse>
    {
        private readonly ICountryApiService _countryApiService;

        public CountryDetailHandler(
            ICountryApiService countryApiService)
        {
            _countryApiService = countryApiService;
        }

        public async Task<GetCountryDetailQueryResponse> HandleAsync(GetCountryDetailQueryRequest request)
        {
            // TODO: Add validator for the request, time permitting.

            var countries = await _countryApiService.GetAllCountriesAsync().ConfigureAwait(false);

            var specificCountry = countries.FirstOrDefault(c => c.Name.Equals(request.CountryName, StringComparison.InvariantCultureIgnoreCase));

            if (specificCountry == null)
            {
                throw new DataNotFoundException(request.CountryName, typeof(CountryDetailDto), ErrorMessages.CountryDetailForNameCouldNotBeFound(request.CountryName));
            }

            return new GetCountryDetailQueryResponse
            {
                Country = specificCountry.ToCountryDetailDto()
            };
        }
    }
}