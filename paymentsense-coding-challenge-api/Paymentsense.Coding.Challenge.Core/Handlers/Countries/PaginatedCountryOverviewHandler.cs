﻿using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Mapping.Countries;
using Paymentsense.Coding.Challenge.Domain.Exceptions;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Core.Handlers.Countries
{
    public class PaginatedCountryOverviewHandler : IRequestHandler<GetPaginatedCountriesQueryRequest, GetPaginatedCountriesQueryResponse>
    {
        private readonly ICountryApiService _countryApiService;
        private readonly IValidator<GetPaginatedCountriesQueryRequest> _validator;

        public PaginatedCountryOverviewHandler(
            ICountryApiService countryApiService, 
            IValidator<GetPaginatedCountriesQueryRequest> validator)
        {
            _countryApiService = countryApiService;
            _validator = validator;
        }

        public async Task<GetPaginatedCountriesQueryResponse> HandleAsync(GetPaginatedCountriesQueryRequest request)
        {
            var validationResult = _validator.Validate(request);

            if (!validationResult.IsValid)
            {
                throw new ValidationRulesException(validationResult.Errors);
            }

            var allCountries = await _countryApiService.GetAllCountriesAsync().ConfigureAwait(false);

            var filteredCountries = allCountries.OrderBy(c => c.Name)
                .Skip((request.PageNumber - 1) * request.ItemsPerPage)
                .Take(request.ItemsPerPage)
                .ToList();

            return new GetPaginatedCountriesQueryResponse
            {
                ItemsPerPage = request.ItemsPerPage,
                TotalItems = allCountries.Count,
                PageNumber = request.PageNumber,
                Countries = filteredCountries.ToCountryOverviewDtoCollection()
            };
        }
    }
}