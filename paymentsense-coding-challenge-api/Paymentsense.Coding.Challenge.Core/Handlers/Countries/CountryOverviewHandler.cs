﻿using System.Threading.Tasks;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Mapping.Countries;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Core.Handlers.Countries
{
    public class CountryOverviewHandler : IRequestHandler<GetCountriesQueryRequest, GetCountriesQueryResponse>
    {
        private readonly ICountryApiService _countryApiService;

        public CountryOverviewHandler(ICountryApiService countryApiService)
        {
            _countryApiService = countryApiService;
        }

        public async Task<GetCountriesQueryResponse> HandleAsync(GetCountriesQueryRequest request)
        {
            var countries = await _countryApiService.GetAllCountriesAsync().ConfigureAwait(false);

            return new GetCountriesQueryResponse { Countries = countries.ToCountryOverviewDtoCollection() };
        }
    }
}