﻿namespace Paymentsense.Coding.Challenge.Core.Services
{
    public interface IJsonSerialisationService
    {
        T Deserialise<T>(string jsonString);
        string Serialise<T>(T dataToSerialise);
        string Serialise(object dataToSerialise);
    }
}