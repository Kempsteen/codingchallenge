﻿using System.Text.Json;

namespace Paymentsense.Coding.Challenge.Core.Services
{
    public class JsonSerialisationService : IJsonSerialisationService
    {
        private readonly JsonSerializerOptions _options;

        public JsonSerialisationService()
        {
            _options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
        }

        public T Deserialise<T>(string jsonString)
        {
            return JsonSerializer.Deserialize<T>(jsonString, _options);
        }

        public string Serialise<T>(T dataToSerialise)
        {
            return JsonSerializer.Serialize(dataToSerialise, _options);
        }

        public string Serialise(object dataToSerialise)
        {
            return JsonSerializer.Serialize(dataToSerialise, _options);
        }
    }
}