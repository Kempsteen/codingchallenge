﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Api.Controllers;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Api.Controllers
{
    [TestFixture]
    public class PaymentsenseCodingChallengeControllerTests
    {
        [Test]
        public void Get_OnInvoke_ReturnsExpectedMessage()
        {
            var controller = new PaymentsenseCodingChallengeController();

            var result = controller.Get().Result as OkObjectResult;

            result.StatusCode.Should().Be(StatusCodes.Status200OK);
            result.Value.Should().Be("Paymentsense Coding Challenge!");
        }
    }
}
