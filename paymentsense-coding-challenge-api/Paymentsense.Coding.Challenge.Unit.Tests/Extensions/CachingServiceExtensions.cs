﻿using System.Reflection;
using LazyCache;
using Microsoft.Extensions.Caching.Memory;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Extensions
{
    public static class CachingServiceExtensions
    {
        public static void Clear(this CachingService cache)
        {
            var cacheProvider = cache.CacheProvider;
            var memoryCache = (MemoryCache)cacheProvider.GetType()
                                                        .GetField("cache", BindingFlags.Instance | BindingFlags.NonPublic)
                                                        .GetValue(cacheProvider);
            
            // ReSharper disable once PossibleNullReferenceException - Won't be null, only used in test code.
            memoryCache.Compact(1.0);
        }
    }
}