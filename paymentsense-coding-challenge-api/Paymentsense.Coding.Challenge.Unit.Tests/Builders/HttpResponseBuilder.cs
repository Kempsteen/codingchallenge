﻿using System.Net;
using System.Net.Http;
using System.Text;
using Paymentsense.Coding.Challenge.Core.Services;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Builders
{
    public class HttpResponseBuilder
    {
        private readonly HttpResponseMessage _response;

        public HttpResponseBuilder()
        {
            _response = new HttpResponseMessage();
        }

        public HttpResponseBuilder WithStatusCode(HttpStatusCode statusCode)
        {
            _response.StatusCode = statusCode;

            return this;
        }

        public HttpResponseBuilder WithRawStringContent(string content)
        {
            _response.Content = new StringContent(content, Encoding.UTF8, "application/json");

            return this;
        }

        public HttpResponseBuilder WithJsonContent<T>(T dataToSerialise)
        {
            _response.Content = new StringContent(new JsonSerialisationService().Serialise(dataToSerialise), Encoding.UTF8, "application/json");

            return this;
        }

        public HttpResponseMessage Build()
        {
            return _response;
        }
    }
}