﻿using System.Collections.Generic;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Data.Countries
{
    public static class CountriesDefaultData
    {
        public static  List<CountryDetailsModel> GetDefaultCountryDetailsOverviewModels()
        {
            var countries = new List<CountryDetailsModel>
            {
                new CountryDetailsModel
                {
                    Flag = "https://restcountries.eu/data/gbr.svg",
                    Name = "United Kingdom of Great Britain and Northern Ireland"
                }
            };

            return countries;
        }
    }
}