﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Services;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Exceptions;
using Paymentsense.Coding.Challenge.Unit.Tests.Builders;
using Paymentsense.Coding.Challenge.Unit.Tests.Data.Countries;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Services
{
    [TestFixture]
    public class CountryApiServiceTests
    {
        private ICountryApiService _countryApiService;

        private Mock<ICountryApiHttpClient> _apiClientMock;
        private IJsonSerialisationService _jsonSerialisationService;

        [SetUp]
        public void SetUp()
        {
            _apiClientMock = new Mock<ICountryApiHttpClient>();
            _jsonSerialisationService = new JsonSerialisationService();

            _countryApiService = new CountryApiService(_apiClientMock.Object, _jsonSerialisationService, new NullLogger<CountryApiService>());
        }

        [Test]
        public void A_Successful_Response_From_The_External_Country_Api_Service_Does_Not_Throw_An_Exception()
        {
            // Arrange
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(
                    new HttpResponseBuilder()
                        .WithStatusCode(HttpStatusCode.OK)
                        .WithJsonContent(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels())
                        .Build());

            // Act and Assert
            Assert.DoesNotThrowAsync(async () => await _countryApiService.GetAllCountriesAsync());
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);
        }

        [Test]
        public async Task A_Successful_Response_From_The_External_Country_Api_Service_Returns_The_Data_Correctly()
        {
            // Arrange
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(
                    new HttpResponseBuilder()
                        .WithStatusCode(HttpStatusCode.OK)
                        .WithJsonContent(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels())
                        .Build());

            // Act
            var result = await _countryApiService.GetAllCountriesAsync();

            // Assert 
            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);

            var expectedCountries = CountriesDefaultData.GetDefaultCountryDetailsOverviewModels();

            Assert.AreEqual(expectedCountries.Count, result.Count, "The number of items returned were incorrect.");

            Assert.Multiple(() =>
            {
                for (var index = 0; index < expectedCountries.Count; index++)
                {
                    Assert.AreEqual(expectedCountries[index].Name, result[index].Name, "The name of the country was not correct.");
                    Assert.AreEqual(expectedCountries[index].Flag, result[index].Flag, "The flag of the country was not correct.");
                }
            });
        }

        [Test]
        public void A_Failed_Response_From_The_External_Country_Api_Service_Throws_An_Exception()
        {
            // Arrange
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(new HttpResponseBuilder()
                    .WithStatusCode(HttpStatusCode.InternalServerError)
                    .WithRawStringContent(GetMockFailedHttpResponseContent()).Build());

            // Act
            var exception = Assert.ThrowsAsync<ExternalSystemException>(async () => await _countryApiService.GetAllCountriesAsync());

            // Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(
                ErrorMessages.ExternalCountryApiFailed(HttpStatusCode.InternalServerError, GetMockFailedHttpResponseContent()).FullMessage, 
                exception.Message,
                "The exception detail was not correct.");

            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);
        }

        [Test]
        public async Task The_External_Api_Is_Called_Multiple_Times_When_Using_The_Non_Cached_Service()
        {
            // Arrange
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(new HttpResponseBuilder()
                    .WithStatusCode(HttpStatusCode.OK)
                    .WithJsonContent(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels())
                    .Build());

            // Act
            var firstCall = await _countryApiService.GetAllCountriesAsync();
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);

            var secondCall = await _countryApiService.GetAllCountriesAsync();
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Exactly(2));

            // Assert
            Assert.AreEqual(firstCall.Count, secondCall.Count);
        }

        private string GetMockFailedHttpResponseContent()
        {
            return "The service is down";
        }
    }
}