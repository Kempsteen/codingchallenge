﻿using System.Net;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Services;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Common;
using Paymentsense.Coding.Challenge.Domain.Exceptions;
using Paymentsense.Coding.Challenge.Unit.Tests.Builders;
using Paymentsense.Coding.Challenge.Unit.Tests.Data.Countries;
using Paymentsense.Coding.Challenge.Unit.Tests.Extensions;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Services
{
    [TestFixture]
    public class CachedCountryApiServiceTests
    {
        private ICountryApiService _countryApiService;

        private Mock<ICountryApiHttpClient> _apiClientMock;
        private IJsonSerialisationService _jsonSerialisationService;

        [SetUp]
        public void SetUp()
        {
            _apiClientMock = new Mock<ICountryApiHttpClient>();
            _jsonSerialisationService = new JsonSerialisationService();
            var cachingService = new CachingService();

            cachingService.Clear();

            _countryApiService = new CachedCountryApiService(
                _apiClientMock.Object, 
                _jsonSerialisationService, 
                new NullLogger<CountryApiService>(),
                cachingService);
        }

        [Test]
        public async Task The_External_Api_Is_Only_Called_Once_When_Using_The_Cached_Service()
        {
            // Arrange
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(new HttpResponseBuilder()
                    .WithStatusCode(HttpStatusCode.OK)
                    .WithJsonContent(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels())
                    .Build());

            // Act
            var firstCall = await _countryApiService.GetAllCountriesAsync();
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);

            var secondCall = await _countryApiService.GetAllCountriesAsync();
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);

            // Assert
            Assert.AreEqual(firstCall.Count, secondCall.Count);
        }

        [Test]
        public async Task An_Exception_Still_Bubbles_Up_And_Is_Not_Cached_When_The_External_Api_Call_Fails_And_Returns_Correct_Data_When_Available()
        {
            // Arrange
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(new HttpResponseBuilder()
                    .WithStatusCode(HttpStatusCode.InternalServerError)
                    .WithRawStringContent("Server failed")
                    .Build());

            // Act
            var exception = Assert.ThrowsAsync<ExternalSystemException>(async () => await _countryApiService.GetAllCountriesAsync());

            // Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(
                ErrorMessages.ExternalCountryApiFailed(HttpStatusCode.InternalServerError, "Server failed").FullMessage,
                exception.Message,
                "The exception detail was not correct.");

            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Once);

            // Change the mock to succeed this call.
            _apiClientMock.Setup(m => m.GetCountriesAsync())
                .ReturnsAsync(new HttpResponseBuilder()
                    .WithStatusCode(HttpStatusCode.OK)
                    .WithJsonContent(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels())
                    .Build());

            var result = await _countryApiService.GetAllCountriesAsync();
            Assert.IsNotNull(result);
            _apiClientMock.Verify(m => m.GetCountriesAsync(), Times.Exactly(2));
        }
    }
}