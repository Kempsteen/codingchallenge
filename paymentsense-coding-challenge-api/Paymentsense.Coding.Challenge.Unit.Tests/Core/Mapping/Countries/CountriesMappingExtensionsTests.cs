﻿using System.Collections.Generic;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi.Models;
using Paymentsense.Coding.Challenge.Core.Mapping.Countries;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Mapping.Countries
{
    [TestFixture]
    public class CountriesMappingExtensionsTests
    {
        [Test]
        public void A_Country_Details_Overview_Model_Maps_Correctly_To_A_Country_Overview_Dto()
        {
            // Arrange
            var model = new CountryDetailsModel
            {
                Name = "Test Country",
                Flag = "Some flag Url"
            };

            // Act
            var result = model.ToCountryOverviewDto();

            // Assert
            Assert.IsNotNull(result);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(model.Name, result.Name);
                Assert.AreEqual(model.Flag, result.FlagUrl);

            });
        }

        [Test]
        public void A_Country_Details_Overview_Model_Collection_Maps_Correctly_To_A_Country_Overview_Dto_Collection()
        {
            // Arrange
            var models = new List<CountryDetailsModel>
            {
                new CountryDetailsModel
                {
                    Name = "Test Country",
                    Flag = "Some flag Url"
                },
                new CountryDetailsModel
                {
                    Name = "Test Country 2",
                    Flag = "Some flag Url 2"
                }
            };

            // Act
            var result = models.ToCountryOverviewDtoCollection();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(models.Count, result.Count);

            Assert.Multiple(() =>
            {
                for (var index = 0; index < models.Count; index++)
                {
                    Assert.AreEqual(models[index].Name, result[index].Name);
                    Assert.AreEqual(models[index].Flag, result[index].FlagUrl);
                }
            });
        }
    }
}