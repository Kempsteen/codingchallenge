﻿using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.Mapping.Countries;
using Paymentsense.Coding.Challenge.Domain.Common;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Mapping.Countries
{
    [TestFixture]
    public class PaginatedCountriesMappingExtensionsTests
    {
        [Test]
        public void A_Paged_Request_Parameters_Maps_Correctly_To_A_Get_Paginated_Countries_Query_Request()
        {
            // Arrange
            var model = new PagedRequestParameters
            {
                ItemsPerPage = 10,
                PageNumber = 5
            };

            // Act
            var result = model.ToGetPaginatedCountriesQueryRequest();

            // Assert
            Assert.IsNotNull(result);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(model.ItemsPerPage, result.ItemsPerPage);
                Assert.AreEqual(model.PageNumber, result.PageNumber);
            });
        }
    }
}