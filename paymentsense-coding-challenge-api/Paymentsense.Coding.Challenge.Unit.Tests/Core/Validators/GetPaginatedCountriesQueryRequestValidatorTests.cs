﻿using System.Linq;
using FluentValidation;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.Validators.Countries;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Validators
{
    [TestFixture]
    public class GetPaginatedCountriesQueryRequestValidatorTests
    {
        private IValidator<GetPaginatedCountriesQueryRequest> _validator;

        [SetUp]
        public void SetUp()
        {
            _validator = new GetPaginatedCountriesQueryRequestValidator();
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-499)]
        public void A_Page_Number_Of_Less_Than_One_Fires_The_Correct_Validation_Message(int pageNumber)
        {
            // Arrange
            var exceptedErrorMessage = ErrorMessages.PageNumberMustBePositive().FullMessage;

            // Act
            var result = _validator.Validate(new GetPaginatedCountriesQueryRequest { PageNumber = pageNumber, ItemsPerPage = 100 });

            // Assert
            Assert.IsFalse(result.IsValid, "Validation should not have succeeded.");
            Assert.AreEqual(1, result.Errors.Count, "Only 1 error should be present.");
            Assert.AreEqual(exceptedErrorMessage, result.Errors.First().ErrorMessage);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-499)]
        public void Items_Per_Page_Of_Less_Than_One_Fires_The_Correct_Validation_Message(int itemsPerPage)
        {
            // Arrange
            var exceptedErrorMessage = ErrorMessages.ItemsPerPageMustBePositive().FullMessage;

            // Act
            var result = _validator.Validate(new GetPaginatedCountriesQueryRequest { PageNumber = 1, ItemsPerPage = itemsPerPage });

            // Assert
            Assert.IsFalse(result.IsValid, "Validation should not have succeeded.");
            Assert.AreEqual(1, result.Errors.Count, "Only 1 error should be present.");
            Assert.AreEqual(exceptedErrorMessage, result.Errors.First().ErrorMessage);
        }

        [Test]
        public void Multiple_Validation_Errors_Are_Returned_When_Multiple_Rules_Are_Broken()
        {
            // Arrange
            var itemsErrorMessage = ErrorMessages.ItemsPerPageMustBePositive().FullMessage;
            var pageNumberErrorMessage = ErrorMessages.PageNumberMustBePositive().FullMessage;

            // Act
            var result = _validator.Validate(new GetPaginatedCountriesQueryRequest { PageNumber = -1, ItemsPerPage = -1 });

            // Assert
            Assert.IsFalse(result.IsValid, "Validation should not have succeeded.");
            Assert.AreEqual(2, result.Errors.Count, "2 errors should be present.");

            Assert.IsTrue(result.Errors.Select(e => e.ErrorMessage).Contains(itemsErrorMessage), 
                $"Error message '{itemsErrorMessage}' was expected but not found.");

            Assert.IsTrue(result.Errors.Select(e => e.ErrorMessage).Contains(pageNumberErrorMessage),
                $"Error message '{pageNumberErrorMessage}' was expected but not found.");
        }

        [Test]
        public void Valid_Request_Returns_No_Errors_And_Is_Valid()
        {
            // Act
            var result = _validator.Validate(new GetPaginatedCountriesQueryRequest { PageNumber = 1, ItemsPerPage = 25 });

            // Assert
            Assert.IsTrue(result.IsValid, "Validation should have succeeded.");
            Assert.IsEmpty(result.Errors);
        }
    }
}