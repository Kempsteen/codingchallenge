﻿using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Handlers;
using Paymentsense.Coding.Challenge.Core.Handlers.Countries;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;
using Paymentsense.Coding.Challenge.Unit.Tests.Data.Countries;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Handlers
{
    [TestFixture]
    public class CountryOverviewHandlerTests
    {
        private IRequestHandler<GetCountriesQueryRequest, GetCountriesQueryResponse> _handler;

        private Mock<ICountryApiService> _countryApiServiceMock;

        [SetUp]
        public void SetUp()
        {
            _countryApiServiceMock = new Mock<ICountryApiService>();
            _countryApiServiceMock.Setup(m => m.GetAllCountriesAsync()).ReturnsAsync(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels);

            _handler = new CountryOverviewHandler(_countryApiServiceMock.Object);
        }

        [Test]
        public async Task Get_Countries_Query_Request_Is_Handled_Correctly()
        {
            // Arrange
            var request = new GetCountriesQueryRequest();

            // Act
            var result = await _handler.HandleAsync(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Countries.Count);

            _countryApiServiceMock.Verify(m => m.GetAllCountriesAsync(), Times.Once);
        }
    }
}