﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Moq;
using NUnit.Framework;
using Paymentsense.Coding.Challenge.Core.External.CountriesApi;
using Paymentsense.Coding.Challenge.Core.Handlers;
using Paymentsense.Coding.Challenge.Core.Handlers.Countries;
using Paymentsense.Coding.Challenge.Domain;
using Paymentsense.Coding.Challenge.Domain.Exceptions;
using Paymentsense.Coding.Challenge.Domain.Queries.Countries;
using Paymentsense.Coding.Challenge.Unit.Tests.Data.Countries;

namespace Paymentsense.Coding.Challenge.Unit.Tests.Core.Handlers
{
    [TestFixture]
    public class PaginatedCountryOverviewHandlerTests
    {
        private IRequestHandler<GetPaginatedCountriesQueryRequest, GetPaginatedCountriesQueryResponse> _handler;

        private Mock<ICountryApiService> _countryApiServiceMock;
        private Mock<IValidator<GetPaginatedCountriesQueryRequest>> _validatorMock;

        [SetUp]
        public void SetUp()
        {
            _countryApiServiceMock = new Mock<ICountryApiService>();
            _validatorMock = new Mock<IValidator<GetPaginatedCountriesQueryRequest>>();
            _countryApiServiceMock.Setup(m => m.GetAllCountriesAsync()).ReturnsAsync(CountriesDefaultData.GetDefaultCountryDetailsOverviewModels);

            _handler = new PaginatedCountryOverviewHandler(_countryApiServiceMock.Object, _validatorMock.Object);
        }

        [Test]
        public async Task A_Valid_Paginated_Get_Countries_Query_Request_Is_Handled_Correctly()
        {
            // Arrange
            var request = new GetPaginatedCountriesQueryRequest { ItemsPerPage = 1, PageNumber = 1 };
            _validatorMock.Setup(m => m.Validate(request)).Returns(new ValidationResult());

            // Act
            var result = await _handler.HandleAsync(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Countries.Count);

            _countryApiServiceMock.Verify(m => m.GetAllCountriesAsync(), Times.Once);
        }

        [Test]
        public void An_Invalid_Paginated_Get_Countries_Query_Request_Is_Handled_Correctly_And_An_Exception_Is_Thrown()
        {
            // Arrange
            var request = new GetPaginatedCountriesQueryRequest { ItemsPerPage = 0, PageNumber = 1 };

            _validatorMock.Setup(m => m.Validate(request))
                .Returns(new ValidationResult(
                    new List<ValidationFailure>
                    {
                        new ValidationFailure("ItemsPerPage", ErrorMessages.ItemsPerPageMustBePositive().FullMessage)
                    }));

            // Act
            var result = Assert.ThrowsAsync<ValidationRulesException>(async () => await _handler.HandleAsync(request));

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual(ErrorMessages.ItemsPerPageMustBePositive().FullMessage, result.Errors.First().FullMessage);

            _countryApiServiceMock.Verify(m => m.GetAllCountriesAsync(), Times.Never);
        }
    }
}