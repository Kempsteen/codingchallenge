import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table'
import { MatPaginatorModule } from '@angular/material/paginator'
import { MatCardModule } from '@angular/material/card'
import { MatListModule } from '@angular/material/list'

@NgModule({
    imports: [
        MatTableModule,
        MatPaginatorModule,
        MatCardModule,
        MatListModule
    ],
    exports: [
        MatTableModule,
        MatPaginatorModule,
        MatCardModule,
        MatListModule
    ]
})
export class AppMaterialModule {}