import { IGetCountryDetailQueryRequest } from 'src/app/models/Queries/get-country-detail-query-request';
import { IGetPaginatedCountriesQueryRequest } from './../../models/Queries/get-paginated-countries-query-request';
import { ICountryOverviewModel } from './../../models/country-overview.model';
import { IGetPaginatedCountriesQueryResponse } from './../../models/Queries/get-paginated-countries-query-response';
import { ICountryDetailModel } from './../../models/country-detail.model';
import { IconService } from './../../services/icon.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { transition, animate, trigger, state, style } from '@angular/animations';
import { CountriesApiService } from 'src/app/services/api/countries-api.service';
import { Observable, Subject } from 'rxjs';

@Component({
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss'],
  animations: [
      trigger('detailExpand', [
          state('collapsed', style({height: '0px', minHeight: '0'})),
          state('expanded', style({height: '*'})),
          transition('expanded <=> collapsed', animate('0.1s ease-out', style({ opacity: 1 })))
      ])
  ]
})
export class CountriesComponent implements OnInit {

    @ViewChild(MatPaginator, {read: MatPaginator, static: false}) paginator: MatPaginator;

    public currentCountries : ICountryOverviewModel[];
    public columnNames = ['name', 'flag', 'detail'];
    public expandedElement : ICountryDetailModel | null;
    public paginatedCountriesResponse : IGetPaginatedCountriesQueryResponse;
    public paginatedCountriesRequest : IGetPaginatedCountriesQueryRequest;

    public pageSize = 10;
    public currentPage = 0;
    public totalSize = 0;
    public pageSearchNumber;

    constructor(
        public iconService : IconService,
        private countriesApiService : CountriesApiService) {}

    ngOnInit(): void {
        this.pageSearchNumber = 1;

        setTimeout(() => {
            this.getCountries();
        })
    }

    public getCountries() : void {
        this.paginatedCountriesRequest = {
            itemsPerPage : this.pageSize,
            pageNumber : this.pageSearchNumber
        };

        this.countriesApiService.getPaginatedCountries(this.paginatedCountriesRequest).subscribe(result => {
            if(!!result && result.succeeded){
                this.paginatedCountriesResponse = result.data;
                this.currentCountries = result.data.countries;
                this.totalSize = result.data.totalItems;
                this.currentPage = result.data.pageNumber - 1;
            }
        }, error => {
            // Would do a snack bar or alert or something given time.
        });
    }

    public handleDetailClick(isExpanded: boolean, name: string) : ICountryDetailModel | null {
        if(isExpanded){
            return null;
        }
        else{
            this.getCountryDetail(name).subscribe((result) =>  {
                return result;
            });
        }
    }

    private getCountryDetail(name : string) : Observable<ICountryDetailModel> {
        let request : IGetCountryDetailQueryRequest = {
            countryName : name
        };

        let subject = new Subject<ICountryDetailModel>();
        this.countriesApiService.getCountryDetail(request).subscribe(result => {
            if(!!result && result.succeeded){
                this.expandedElement = result.data.country;
            }
            subject.next(this.expandedElement);
        },
        error => {
            subject.next(this.expandedElement);
        });

        return subject.asObservable();
    }
  
    public handlePage($event: PageEvent) : void{
        this.currentPage = $event.pageIndex;
        this.pageSize = $event.pageSize;
        this.paginatedCountriesRequest.pageNumber = this.currentPage + 1;

        this.countriesApiService.getPaginatedCountries(this.paginatedCountriesRequest).subscribe(result => {
            if(!!result && result.succeeded){
                this.paginatedCountriesResponse = result.data;
                this.currentCountries = result.data.countries;
                this.totalSize = result.data.totalItems;
                this.currentPage = result.data.pageNumber - 1;
            }
        }, error => {
            // Would do a snack bar or alert or something given time.
        });
    }
}
    