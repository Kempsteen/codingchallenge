import { Component, Input } from '@angular/core';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'paymentsense-page-title',
    templateUrl: './page-title.component.html',
    styleUrls: ['./page-title.component.scss']
  })

  export class PageTitleComponent {
    @Input() title : string;
    @Input() iconDefinition : IconDefinition;

    constructor(){
    }
  }