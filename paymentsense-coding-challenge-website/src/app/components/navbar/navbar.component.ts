import { IconService } from '../../services/icon.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'paymentsense-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss' ]
  })

  export class NavbarComponent implements OnInit {
    isCollapsed = true;

    constructor(
      public router : Router,
      public iconService : IconService){ }

    ngOnInit(): void {
        
    }
  }