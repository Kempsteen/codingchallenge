import { AppMaterialModule } from './app-material.module';
import { IconService } from './services/icon.service';
import { CountriesComponent } from './components/countries/countries.component';
import { PageTitleComponent } from './components/shared/page-title/page-title.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { EnvironmentService } from './services/environment.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PaymentsenseCodingChallengeApiService } from './services/paymentsense-coding-challenge-api.service';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// NGX Bootstrap
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CountriesApiService } from './services/api/countries-api.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    PageTitleComponent,
    CountriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FontAwesomeModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    AppMaterialModule
  ],
  providers: [
    PaymentsenseCodingChallengeApiService,
    EnvironmentService,
    IconService,
    CountriesApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
