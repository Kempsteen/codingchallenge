export interface IErrorModel{
    code: string;
    message: string;
    fullMessage: string;
}