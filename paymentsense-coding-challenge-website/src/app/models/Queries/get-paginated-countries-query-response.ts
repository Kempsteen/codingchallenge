import { ICountryOverviewModel } from './../country-overview.model';

export interface IGetPaginatedCountriesQueryResponse {
    countries : ICountryOverviewModel[];
    pageNumber: number;
    itemsPerPage: number;
    totalItems: number;
    totalPages: number;
    itemsFrom: number;
    itemsTo: number;
}