import { ICountryDetailModel } from '../country-detail.model';

export interface IGetCountryDetailQueryResponse {
    country: ICountryDetailModel;
}