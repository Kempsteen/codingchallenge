export interface IGetPaginatedCountriesQueryRequest {
    pageNumber: number;
    itemsPerPage: number;
}