import { IErrorModel } from './error.model';

export interface IErrorResponse{
    exceptionType : string;
    errors: IErrorModel[];
}