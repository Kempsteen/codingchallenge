export interface ICountryOverviewModel {
    name: string;
    flagUrl: string;
}