import { IErrorResponse } from './error-response.model';

export interface IApiResponse<T>{
    statusCode: number;
    succeeded: boolean;
    data: T;
    errorResponse: IErrorResponse;
}