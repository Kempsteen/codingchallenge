export interface ICountryDetailModel {
    name: string;
    flagUrl: string;
    population: number;
    languages: string[];
    capital: string;
    borders: string[];
}