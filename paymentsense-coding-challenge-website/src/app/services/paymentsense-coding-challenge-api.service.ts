import { EnvironmentService } from './environment.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentsenseCodingChallengeApiService {
  constructor(private httpClient: HttpClient,
    private environmentService: EnvironmentService) {}

  public getHealth(): Observable<string> {
    return this.httpClient.get(`${this.environmentService.apiUrl}/health`, { responseType: 'text' });
  }
}
