import { IApiResponse } from './../../models/api-response.model';
import { Observable, throwError } from 'rxjs';
import { IGetPaginatedCountriesQueryRequest } from './../../models/Queries/get-paginated-countries-query-request';
import { EnvironmentService } from './../environment.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IGetPaginatedCountriesQueryResponse } from 'src/app/models/Queries/get-paginated-countries-query-response';
import { catchError, timeout } from 'rxjs/operators';
import { IGetCountryDetailQueryRequest } from 'src/app/models/Queries/get-country-detail-query-request';
import { IGetCountryDetailQueryResponse } from 'src/app/models/Queries/get-country-detail-query-response';

@Injectable({
  providedIn: 'root',
})

export class CountriesApiService {
    
    constructor(
        private httpClient: HttpClient,
        private environmentService: EnvironmentService){}

    getPaginatedCountries(request: IGetPaginatedCountriesQueryRequest) : Observable<IApiResponse<IGetPaginatedCountriesQueryResponse>> { 
        return this.httpClient.get<IApiResponse<IGetPaginatedCountriesQueryResponse>>(
            `${this.environmentService.apiUrl}/api/countries/paginated?PageNumber=${request.pageNumber}&ItemsPerPage=${request.itemsPerPage}`)
            .pipe(
                timeout(30000), 
                catchError((error: HttpErrorResponse) => throwError(error || 'Server error'))
                );
    }

    getCountryDetail(request: IGetCountryDetailQueryRequest) : Observable<IApiResponse<IGetCountryDetailQueryResponse>> {
        return this.httpClient.get<IApiResponse<IGetCountryDetailQueryResponse>>(
            `${this.environmentService.apiUrl}/api/countries/${request.countryName}`)
            .pipe(
                timeout(30000), 
                catchError((error: HttpErrorResponse) => throwError(error || 'Server error'))
                );
    }
}