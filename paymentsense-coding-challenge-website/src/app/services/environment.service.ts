import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class EnvironmentService {
    get production() : boolean {
        return environment.production;
    }

    get apiUrl() : string {
        return environment.apiUrl;
    }
}