import { Injectable } from '@angular/core';
import { faFlag, IconDefinition, faChevronCircleUp, faChevronCircleDown } from '@fortawesome/free-solid-svg-icons';

@Injectable({
  providedIn: 'root',
})

export class IconService {
    
    get countries(): IconDefinition {
        return faFlag;
    }

    get upArrow(): IconDefinition {
        return faChevronCircleUp;
    }

    get downArrow(): IconDefinition {
        return faChevronCircleDown;
    }
}